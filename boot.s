[bits 16]
[org 0x7c00]
_entry:
	jmp 0:main
	times 8-($-$$) db 0

boot_information_table:
.primary_volume_descriptor:
	resd 1
.boot_file_location:
	resd 1
.boot_file_lenght:
	resd 1
.checksum:
	resd 1
.reserved:
	resb 40

main:
	xor ax, ax
	mov ds, ax
	mov ss, ax
	mov sp, 0x7c00
	cld

	mov ax, 99
.loop:
		inc BYTE [text_color]

		; first line
		call print_number
		mov si, msg1
		call print_string
		call print_number
		mov si, msg2
		call print_string

		dec ax

		; second line
		mov si, msg3
		call print_string
		call print_number
		mov si, msg4
		call print_string

		call sleep

		test ax, ax
		jnz .loop

.hang:
	cli
	hlt
	jmp .hang

msg1:
	db " Bottles of beer on the wall, ", 0
msg2:
	db " bottles of beer.", 13, 10, 0
msg3:
	db "Take one down, pass it around, ", 0
msg4:
	db " bottles of beer on the wall ...", 13, 10, 13, 10, 0
const10:
	dw 10
text_color:
	db 0

sleep:
	pusha
	mov ah, 0x86
	mov cx, 5
	mov dx, 0
	int 0x15
	popa
	ret

putc:
	pusha
	mov ah, 0x0B
	mov bh, 0
	mov bl, [text_color]
	int 0x10
	popa
	pusha
	mov ah, 0x0E
	mov bh, 0
;	mov bl, [ text_color ]
	int 0x10
	popa
	ret

print_number:
	pusha
	xor dx, dx
	div WORD [const10]
	test ax, ax
	je .end
	call print_number
.end:
	mov ax, dx
	add ax, '0'
	call putc
	popa
	ret

print_string:
	pusha

.loop:
	lodsb
	or al, al
	jz .end
	call putc
	jmp .loop
.end:
	popa
	ret

	times 510 - ($ - $$) db 0
	db 0x55
	db 0xAA
