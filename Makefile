.DEFAULT: run
.PHONY: run
run: boot
	qemu-system-i386 boot

boot: boot.s
	nasm boot.s -o boot

boot.iso: boot
	cp $< iso/boot
	xorriso -as mkisofs -R -J -c bootcat -b boot -no-emul-boot -boot-load-size 1 -o boot.iso iso

.PHONY: clean
clean:
	rm -f iso/boot
	rm -f boot boot.img
